package com.alecbrando.homeworkrecyclerview.util

import com.alecbrando.homeworkrecyclerview.model.models.Data

sealed class ResourceWrapper(data: List<Data>?, errorMsg: String?) {
    data class Success(val data: List<Data>) : ResourceWrapper(data, null)
    object Loading : ResourceWrapper(null, null)
    data class Error(val errorMsg: String?) : ResourceWrapper(null, errorMsg)
}



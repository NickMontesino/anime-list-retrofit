package com.alecbrando.homeworkrecyclerview.model.models

data class Small(
    val height: Int,
    val width: Int
)
package com.alecbrando.homeworkrecyclerview.model

import com.alecbrando.homeworkrecyclerview.model.models.Animes
import com.alecbrando.homeworkrecyclerview.model.models.Data
import com.google.gson.annotations.SerializedName
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface API {

    data class AnimeResponse(
        @SerializedName("data")
        val animeList: List<Data>
    )

    @GET("anime")
    suspend fun fetchAnimes(): Response<AnimeResponse>

    companion object {
        val retrofitInstance by lazy {
            Retrofit
                .Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://kitsu.io/api/edge/")
                .build()
                .create(API::class.java)
        }
    }

}